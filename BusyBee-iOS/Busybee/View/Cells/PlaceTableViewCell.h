//
//  PlaceTableViewCell.h
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWLabel.h"
#import "Place.h"

@interface PlaceTableViewCell : UITableViewCell {
    IBOutlet UIImageView*       imvImage;
    IBOutlet UILabel*           lblTitle;
    IBOutlet UILabel*           lblBusy;
    IBOutlet UILabel*           lineBusy;
    IBOutlet RWLabel*           lblAddress;
    IBOutlet UILabel*           lblPhone;
    IBOutlet UILabel*           lblOpenHours;
    
    IBOutlet UIButton*          btnDirections;
    IBOutlet UIButton*          btnWriteReview;
}

- (void) setPlace:(Place*) place;

@end
