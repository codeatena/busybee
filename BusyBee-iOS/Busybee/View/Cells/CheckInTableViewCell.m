//
//  CheckInTableViewCell.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "CheckInTableViewCell.h"
#import "Constants.h"
#import "Level.h"

@implementation CheckInTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [btnDiretion setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnWriteReview setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    
    [btnDiretion setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    [btnWriteReview setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    [lblCheckUserName setTextColor:UIColorFromRGB(GRAY_COLOR)];
    [lblCheckTime setTextColor:UIColorFromRGB(GRAY_COLOR)];
    
    lblPlaceStatus.layer.cornerRadius = 10.0f;
    lblPlaceStatus.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setCheckin:(Checkin*) checkin {
    
    lblCheckUserName.text = [NSString stringWithFormat:@"Check-in by %@", checkin.user.name];
    lblCheckTime.text = [checkin.date getDateString];
    lblCheckPlaceTitle.text = checkin.place.title;
    lblCheckPlaceAddress.text = checkin.place.address;
    
    lblPlaceStatus.backgroundColor = [Level getBusyColor:checkin.place.nLevel];
    linePlaceStatus.backgroundColor = [Level getBusyColor:checkin.place.nLevel];
}

@end
