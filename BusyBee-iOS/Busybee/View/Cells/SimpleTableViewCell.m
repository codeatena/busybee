//
//  SimpleTableViewCell.m
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "SimpleTableViewCell.h"

@implementation SimpleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setSimpleData:(SimpleData*) simpleData {
    lblTitle.text = simpleData.title;
    lblDesc.text = simpleData.desc;
}

@end
