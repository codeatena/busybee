//
//  CheckInTableViewCell.h
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Checkin.h"

@interface CheckInTableViewCell : UITableViewCell {
    IBOutlet UILabel*           lblCheckUserName;
    IBOutlet UILabel*           lblCheckTime;
    IBOutlet UILabel*           lblCheckPlaceTitle;
    IBOutlet UILabel*           lblCheckPlaceAddress;
    IBOutlet UIButton*          btnDiretion;
    IBOutlet UIButton*          btnWriteReview;
    IBOutlet UILabel*           lblPlaceStatus;
    IBOutlet UILabel*           linePlaceStatus;
}

- (void) setCheckin:(Checkin*) checkin;

@end