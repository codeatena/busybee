//
//  BuzzingTableViewCell.m
//  Busybee
//
//  Created by User on 4/18/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "BuzzingTableViewCell.h"
#import "Level.h"

@implementation BuzzingTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [lblTitle setTextColor:UIColorFromRGB(BLACK_COLOR)];
    
    [btnDirections setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnDirections setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    [btnWriteReview setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnWriteReview setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    lblStatus.layer.cornerRadius = 10.0f;
    lblStatus.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setPlace:(Place*) _place {
    lblTitle.text = _place.title;
    lblAddress.text = _place.address;
    lblePhone.text = _place.displayPhone;
    lblOpentime.text = [_place getTodayOpenTime];
    [lblStatus setBackgroundColor:[Level getBusyColor:_place.nLevel]];
    [lineStatus setBackgroundColor:[Level getBusyColor:_place.nLevel]];
}

@end
