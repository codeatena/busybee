//
//  FriendTableViewCell.h
//  Busybee
//
//  Created by User on 4/19/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface FriendTableViewCell : UITableViewCell {
    
    IBOutlet UIImageView*   imvPhoto;
    IBOutlet UILabel* lblNameAddress;
    IBOutlet UILabel* lblReviews;
    IBOutlet UILabel* lblFriends;
    IBOutlet UILabel* lblPoints;
    IBOutlet UILabel* lblRank;
    IBOutlet UILabel* lblStatus;
    IBOutlet UILabel* lineStatus;
    
    IBOutlet UIButton* btnRecommendPlace;
}

- (void) setFriend:(User*) user;

@end
