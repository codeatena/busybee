//
//  PlaceTableViewCell.m
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "PlaceTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "Level.h"

@implementation PlaceTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [btnDirections setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnWriteReview setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    
    [btnDirections setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    [btnWriteReview setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    lblBusy.layer.cornerRadius = 10.0f;
    lblBusy.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setPlace:(Place*) place {
    
    imvImage.image = [UIImage imageNamed:place.strImageFileName];
    
    if (place.strImagePath != nil && ![place.strImagePath isEqualToString:@""]) {
        NSURL *imgUrl = [[NSURL alloc] initWithString:place.strImagePath];
        
        [imvImage sd_setImageWithURL:imgUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    
    lblTitle.text = place.title;

    lblBusy.backgroundColor = [Level getBusyColor:place.nLevel];
    lineBusy.backgroundColor = [Level getBusyColor:place.nLevel];
    
    lblAddress.text = place.address;
    
    lblPhone.text = place.displayPhone;
    lblOpenHours.text = [place getTodayOpenTime];
}

@end