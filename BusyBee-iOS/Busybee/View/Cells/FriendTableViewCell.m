//
//  FriendTableViewCell.m
//  Busybee
//
//  Created by User on 4/19/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "FriendTableViewCell.h"
#import "Level.h"

@implementation FriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [btnRecommendPlace setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnRecommendPlace setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    lblStatus.layer.cornerRadius = 10.0f;
    lblStatus.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setFriend:(User*) user {
    imvPhoto.image = [UIImage imageNamed:user.strImageFileName];
    NSString* strNameAddress = [NSString stringWithFormat:@"%@ | %@", user.name, user.address];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:strNameAddress];
    
    [attributedText addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:15.0]
                  range:NSMakeRange(0, user.name.length)];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont systemFontOfSize:12.0]
                           range:NSMakeRange(user.name.length, strNameAddress.length - user.name.length)];
    
    [attributedText addAttribute:NSForegroundColorAttributeName
                   value:UIColorFromRGB(GRAY_COLOR)
                   range:NSMakeRange(user.name.length, strNameAddress.length - user.name.length)];
    
    lblNameAddress.attributedText = attributedText;
    lblReviews.text = [NSString stringWithFormat:@"%lu Reviews", (unsigned long) user.arrReviews.count];
    lblFriends.text = [NSString stringWithFormat:@"%lu Friends", (unsigned long) user.arrFriends.count];
    lblPoints.text = [NSString stringWithFormat:@"%lu Points", (unsigned long) user.nPoint];
    lblRank.text = [NSString stringWithFormat:@"Rank %lu", (unsigned long) user.nRank];
    
    [lblStatus setBackgroundColor:[Level getBusyColor:user.nLevel]];
    [lineStatus setBackgroundColor:[Level getBusyColor:user.nLevel]];
}

@end
