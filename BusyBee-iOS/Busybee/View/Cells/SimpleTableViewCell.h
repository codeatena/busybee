//
//  SimpleTableViewCell.h
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleData.h"

@interface SimpleTableViewCell : UITableViewCell {
    IBOutlet UILabel*   lblTitle;
    IBOutlet UILabel*   lblDesc;
}

- (void) setSimpleData:(SimpleData*) simpleData;

@end
