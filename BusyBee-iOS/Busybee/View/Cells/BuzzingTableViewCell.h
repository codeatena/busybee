//
//  BuzzingTableViewCell.h
//  Busybee
//
//  Created by User on 4/18/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"

@interface BuzzingTableViewCell : UITableViewCell {
    IBOutlet UILabel*   lblTitle;
    IBOutlet UILabel*   lblAddress;
    IBOutlet UILabel*   lblePhone;
    IBOutlet UILabel*   lblOpentime;
    IBOutlet UILabel*   lblStatus;
    IBOutlet UILabel*   lineStatus;
    
    IBOutlet UIButton*  btnDirections;
    IBOutlet UIButton*  btnWriteReview;
}

- (void) setPlace:(Place*) _place;

@end
