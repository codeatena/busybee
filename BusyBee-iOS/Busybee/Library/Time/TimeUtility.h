//
//  TimeUtility.h
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeUtility : NSObject

+ (NSString*) getDateStringFromaDate:(NSDate*) date;

@end
