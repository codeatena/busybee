//
//  CustomDate.h
//  Busybee
//
//  Created by User on 4/14/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomDate : NSObject

@property (nonatomic, strong) NSDate* date;

- (NSString*) getDateString;

@end
