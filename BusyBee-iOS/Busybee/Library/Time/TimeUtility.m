//
//  TimeUtility.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "TimeUtility.h"

@implementation TimeUtility

+ (NSString*) getDateStringFromaDate:(NSDate*) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    return [dateFormatter stringFromDate:date];
}

@end