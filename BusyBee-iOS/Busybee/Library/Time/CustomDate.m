//
//  CustomDate.m
//  Busybee
//
//  Created by User on 4/14/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "CustomDate.h"
#import "TimeUtility.h"

@implementation CustomDate

@synthesize date;

- (id) init {
    self = [super init];
    if (self) {
        date = [NSDate date];
    }
    return self;
}

- (NSString*) getDateString {
    return [TimeUtility getDateStringFromaDate:date];
}

@end
