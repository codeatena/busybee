//
//  NumberUtility.h
//  Busybee
//
//  Created by User on 4/17/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NumberUtility : NSObject

+ (NSString*) getFormattedStringFromInteger:(NSInteger) nValue;

@end
