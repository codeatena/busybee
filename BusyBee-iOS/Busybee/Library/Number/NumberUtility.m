//
//  NumberUtility.m
//  Busybee
//
//  Created by User on 4/17/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "NumberUtility.h"

@implementation NumberUtility

+ (NSString*) getFormattedStringFromInteger:(NSInteger) nValue {
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle];
    [fmt setMaximumFractionDigits:0];
    
    NSString *result = [fmt stringFromNumber:@(nValue)];
    
    return result;
}

@end
