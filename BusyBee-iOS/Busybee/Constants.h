//
//  Constants.h
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#ifndef Busybee_Constants_h
#define Busybee_Constants_h

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define YELLOW_COLOR            0xffde16
#define BLACK_COLOR             0x231f20
#define GRAY_COLOR              0x595858
#define DARK_GRAY_COLOR         0x2c2728

#define SLIDER_BORDER_COLOR     0xe4e3e2
#define SLIDER_BACK_COLOR       0xb5b5b5

#define MENU_BACK_COLOR         0x1b1919
#define MENU_TEXT_COLOR         0xefefef

#define BABY_COLOR              0x2ecc71
#define DRONE_COLOR             0xdfbd11
#define WORKER_COLOR            0xe67e22
#define BUMBLE_COLOR            0xbd4f33
#define QUEEN_COLOR             0xc0392b

#define NOTIFY_MENU_CLICK_HOME          @"notify_menu_click_home"
#define NOTIFY_MENU_CLICK_PROFILE       @"notify_menu_click_profile"
#define NOTIFY_MENU_CLICK_RANKING       @"notify_menu_click_ranking"
#define NOTIFY_MENU_CLICK_FAVORITES     @"notify_menu_click_favorites"
#define NOTIFY_MENU_CLICK_FRIENDS       @"notify_menu_click_friends"
#define NOTIFY_MENU_CLICK_BUZZING       @"notify_menu_click_buzzing"
#define NOTIFY_MENU_CLICK_FEED          @"notify_menu_click_feed"
#define NOTIFY_MENU_CLICK_MAP           @"notify_menu_click_map"
#define NOTIFY_MENU_CLICK_TUTORIALS     @"notify_menu_click_tutorials"

//typedef enum MENU_ITEMS {
//    MENU_ITEM_HOME,
//    MENU_ITEM_PROFILE,
//    MENU_ITEM_POINTS,
//    MENU_ITEM_FAVOURITES,
//    MENU_ITEM_FROEMDS,
//    MENU_ITEM_BUZZING,
//    MENU_ITEM_FEED,
//    MENU_ITEM_MAP,
//    MENU_ITEM_TUTORIALS
//} MENU_ITEMS;

typedef enum BUSY_LEVEL {
    BABY,
    DRONE,
    WORKER,
    BUMBLE,
    QUEEN
} BUSY_LEVEL;

#define RANK_CELL_HEIGHT 31.0f

#endif