//
//  Review.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "Review.h"

@implementation Review

@synthesize user, place, text;

- (id) init {
    self = [super init];
    if (self) {
        user = [[User alloc] init];
        place = [[Place alloc] init];
        text = @"";
    }
    return self;
}

@end