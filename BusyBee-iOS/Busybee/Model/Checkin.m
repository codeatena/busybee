//
//  Checkin.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "Checkin.h"

@implementation Checkin

@synthesize user, place, date;

- (id) init {
    self = [super init];
    if (self) {
        user = [[User alloc] init];
        place = [[Place alloc] init];
        date = [[CustomDate alloc] init];
    }
    return self;
}

@end