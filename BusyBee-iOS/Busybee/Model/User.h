//
//  User.h
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <Parse/Parse.h>

@interface User : NSObject

@property (nonatomic, strong)   NSString*           email;
@property (nonatomic, strong)   NSString*           firstName;
@property (nonatomic, strong)   NSString*           lastName;
@property (nonatomic, strong)   NSString*           name;
@property (nonatomic, strong)   NSString*           address;
@property (nonatomic, strong)   NSString*           phone;
@property (nonatomic, strong)   NSMutableArray*     arrReviews;
@property (nonatomic, strong)   NSMutableArray*     arrFriends;
@property (nonatomic)           NSUInteger          nPoint;
@property (nonatomic)           NSUInteger          nRank;
@property (nonatomic)           NSUInteger          nWorldRank;
@property (nonatomic)           NSUInteger          nBadge;
@property (nonatomic, strong)   NSMutableArray*     arrCheckin;
@property (nonatomic)           BUSY_LEVEL          nLevel;
@property (nonatomic, strong)   NSString*           strImageFileName;

@property (nonatomic, strong)   PFUser*             pUser;

- (void) setPUser:(PFUser*) _pUser;

@end