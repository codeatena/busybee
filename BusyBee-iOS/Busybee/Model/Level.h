//
//  Level.h
//  Busybee
//
//  Created by User on 4/17/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <UIKit/UIKit.h>

@interface Level : NSObject

@property (nonatomic) BUSY_LEVEL nLevel;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* desc;

+ (NSString*) getTitle:(BUSY_LEVEL) _nLevel;
+ (NSString*) getDesc:(BUSY_LEVEL) _nLevel;
+ (UIColor*) getBusyColor:(BUSY_LEVEL) nLevel;

@end
