//
//  Place.h
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"

@interface OpenTime : NSObject

@property (nonatomic, strong) NSString* startTime;
@property (nonatomic, strong) NSString* endTime;

@end

@interface Place : NSObject

@property (nonatomic, strong)   NSString* title;
@property (nonatomic, strong)   NSString* callPhone;
@property (nonatomic, strong)   NSString* strImagePath;
@property (nonatomic, strong)   NSString* displayPhone;
@property (nonatomic, strong)   NSString* address;
@property (nonatomic, strong)   NSMutableArray* arrOpenTime;
@property (nonatomic, strong)   NSString* strImageFileName;
@property (nonatomic)           BUSY_LEVEL  nLevel;

@property (nonatomic)           double lat;
@property (nonatomic)           double lng;

- (NSString*) getTodayOpenTime;

@end