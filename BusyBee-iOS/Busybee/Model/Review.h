//
//  Review.h
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Place.h"

@interface Review : NSObject

@property (nonatomic, strong) User* user;
@property (nonatomic, strong) Place* place;
@property (nonatomic, strong) NSString* text;

@end
