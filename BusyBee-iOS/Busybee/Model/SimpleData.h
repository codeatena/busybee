//
//  SimpleData.h
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleData : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* desc;

- (id) initWithTitle:(NSString*) _title DESC:(NSString*) _desc;

@end
