//
//  SimpleData.m
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "SimpleData.h"

@implementation SimpleData

@synthesize title, desc;

- (id) init {
    self = [super init];
    if (self) {
        title = @"";
        desc = @"";
    }
    return self;
}

- (id) initWithTitle:(NSString*) _title DESC:(NSString*) _desc {
    self = [self init];
    if (self) {
        title = _title;
        desc = _desc;
    }
    return self;
}

@end
