//
//  Place.m
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "Place.h"
#import "Constants.h"

@implementation OpenTime

@synthesize startTime, endTime;

- (id) init {
    self = [super init];
    if (self) {
        startTime = @"";
        endTime = @"";
    }
    return self;
}

@end

@implementation Place

@synthesize title, callPhone, displayPhone, arrOpenTime, nLevel, strImagePath, address, strImageFileName;
@synthesize lat, lng;

- (id) init {
    self = [super init];
    if (self) {
        arrOpenTime = [[NSMutableArray alloc] init];
        nLevel = BABY;
        strImagePath = @"";
        title = @"";
        callPhone = @"";
        displayPhone = @"";
        address = @"";
        strImageFileName = @"";
        lat = 0.0f;
        lng = 0.0f;
    }
    return self;
}

- (NSString*) getTodayOpenTime {
    NSString* str = @"Open today: 10:30 am - 10:00 pm";
    return str;
}

@end