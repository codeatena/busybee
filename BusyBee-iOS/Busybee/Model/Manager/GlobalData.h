//
//  GlobalData.h
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface GlobalData : NSObject

@property (nonatomic, strong) User* curUser;
@property (nonatomic, strong) NSMutableArray* arrPlaces;

@property (nonatomic)         int nLocalUserCount;
@property (nonatomic)         int nAllUserCount;

@property (nonatomic)         BOOL isNewUserbyFacebook;

+ (instancetype) sharedInstance;

@end