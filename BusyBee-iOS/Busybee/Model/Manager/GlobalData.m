//
//  GlobalData.m
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "GlobalData.h"
#import "SampleDataManager.h"

@implementation GlobalData

@synthesize curUser, arrPlaces, nLocalUserCount, nAllUserCount, isNewUserbyFacebook;

+ (instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id) init {
    self = [super init];
    if (self) {
        curUser = [[User alloc] init];
        arrPlaces = [[NSMutableArray alloc] init];
        nLocalUserCount = 0;
        nAllUserCount = 0;
        isNewUserbyFacebook = NO;
        [self makeSampleData];
    }
    return self;
}

- (void) makeSampleData {
    arrPlaces = [[SampleDataManager sharedInstance] getSamplePlaces];
    curUser = [[SampleDataManager sharedInstance] getSampleCurUser];
    
    nLocalUserCount = 11000;
    nAllUserCount = 119000;
}

@end