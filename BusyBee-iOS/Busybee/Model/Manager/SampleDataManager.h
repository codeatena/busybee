//
//  SampleDataManager.h
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface SampleDataManager : NSObject

+ (instancetype) sharedInstance;

- (NSMutableArray*) getSamplePlaces;
- (User*) getSampleCurUser;
- (NSMutableArray*) getSampleReviews;
- (NSMutableArray*) getSampleFriends;
- (NSMutableArray*) getSampleCheckins:(User*) user;

@end