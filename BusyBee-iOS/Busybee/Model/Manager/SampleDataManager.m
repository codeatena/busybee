//
//  SampleDataManager.m
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "SampleDataManager.h"
#import "GlobalData.h"
#import "Checkin.h"

@implementation SampleDataManager

+ (instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSMutableArray*) getSamplePlaces {
    
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    Place* place = nil;
    
    place = [[Place alloc] init];
    place.title = @"Jade Dynasty Sea Food Restaurant";
    place.displayPhone = @"(808) 947-8818";
    place.callPhone = @"8089478818";
    place.nLevel = WORKER;
    place.address = @"1450 Ala Moana Blvd #4220, Honolulu, HI 96814, United States";
    place.strImageFileName = @"pic1.png";
    place.lat = 21.2912881f;
    place.lng = - 157.8429647f;
    [arrData addObject:place];
    
    place = [[Place alloc] init];
    place.title = @"Honolulu International Airport";
    place.displayPhone = @"(808) 836-6411";
    place.callPhone = @"8088366411";
    place.nLevel = BABY;
    place.address = @"300 Rodgers Blvd, Honolulu, HI 96819, United States";
    place.strImageFileName = @"pic2.png";
    place.lat = 21.331396;
    place.lng = -157.929492;
    [arrData addObject:place];
    
    place = [[Place alloc] init];
    place.title = @"GREENS AND VINES";
    place.displayPhone = @"(808) 536-9680";
    place.callPhone = @"8085369680";
    place.address = @"909 Kapiolani Blvd Unit B Honolulu, HI 96814, United States";
    place.strImageFileName = @"pic3.png";
    place.nLevel = QUEEN;
    place.lat = 21.2986962;
    place.lng = -157.855032;
    [arrData addObject:place];
    
    return arrData;
}

- (User*) getSampleCurUser {
    User* user = [[User alloc] init];
    user.firstName = @"John";
    user.lastName = @"Stevens";
    user.address = @"Honolulu, Hawaii";
    user.arrReviews = [self getSampleReviews];
    user.arrFriends = [self getSampleFriends];
    user.nPoint = 1023;
    user.nRank = 120;
    user.nWorldRank = 1998;
    user.nBadge = 33;
    user.nLevel = WORKER;
    user.arrCheckin = [self getSampleCheckins:user];
    return user;
}

- (NSMutableArray*) getSampleReviews {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    return arrData;
}

- (NSMutableArray*) getSampleFriends {
    
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    User* user = nil;
    
    user = [[User alloc] init];
    user.firstName = @"Gavin";
    user.lastName = @"Parker";
    user.address = @"Honolulu, Hawaii";
    user.nPoint = 2045;
    user.nRank = 56;
    user.nLevel = QUEEN;
    user.strImageFileName = @"friend1.png";
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.firstName = @"Jim";
    user.lastName = @"Rivera";
    user.address = @"Honolulu, Hawaii";
    user.nPoint = 2045;
    user.nRank = 56;
    user.nLevel = BABY;
    user.strImageFileName = @"friend2.png";
    [arrData addObject:user];

    user = [[User alloc] init];
    user.firstName = @"Diane";
    user.lastName = @"Diaz";
    user.address = @"Honolulu, Hawaii";
    user.nPoint = 2045;
    user.nRank = 56;
    user.nLevel = WORKER;
    user.strImageFileName = @"friend3.png";
    [arrData addObject:user];

    user = [[User alloc] init];
    user.firstName = @"Raymond";
    user.lastName = @"Ford";
    user.address = @"Honolulu, Hawaii";
    user.nPoint = 2045;
    user.nRank = 56;
    user.nLevel = DRONE;
    user.strImageFileName = @"friend4.png";
    [arrData addObject:user];
    
    return arrData;
}

- (NSMutableArray*) getSampleCheckins:(User*) user {

    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    NSMutableArray* arrPlaces = [self getSamplePlaces];
    for (Place* place in arrPlaces) {
        Checkin* checkin = [[Checkin alloc] init];
        checkin.user = user;
        checkin.place = place;
        [arrData addObject:checkin];
    }
    return arrData;
}

@end
