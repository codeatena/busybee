//
//  User.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize email, firstName, lastName, name, address, phone, arrReviews, arrFriends, nPoint, nRank, nWorldRank, nBadge, arrCheckin, nLevel, strImageFileName, pUser;

- (id) init {
    self = [super init];
    if (self) {
        email = @"";
        firstName = @"";
        lastName = @"";
        name = @"";
        address = @"";
        strImageFileName = @"avatar.jpg";
        
        arrReviews = [[NSMutableArray alloc] init];
        arrFriends = [[NSMutableArray alloc] init];
        nPoint = 0;
        nRank = 0;
        nWorldRank = 0;
        nBadge = 0;
        nLevel = BABY;
        arrCheckin = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSString*) name {
    if ([name isEqualToString:@""]) {
        return [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    }
    return name;
}

- (void) setPUser:(PFUser*) _pUser {
    pUser = _pUser;
    email = pUser.email;
    firstName = pUser[@"firstName"];
    lastName = pUser[@"lastName"];
    phone = pUser[@"phone"];
}

@end
