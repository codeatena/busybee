//
//  Level.m
//  Busybee
//
//  Created by User on 4/17/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "Level.h"

@implementation Level

@synthesize nLevel, title, desc;

- (id) init {
    self = [super init];
    if (self) {
        nLevel = WORKER;
        title = @"Busy Bee";
        desc = @"Welcome to Busy Bees!";
    }
    return self;
}

- (id) initWithBusyType:(BUSY_LEVEL) _nLevel {
    self = [self init];
    if (self) {
        nLevel = _nLevel;
        title = [Level getTitle:_nLevel];
        desc = [Level getDesc:nLevel];
    }
    return self;
}

+ (NSString*) getTitle:(BUSY_LEVEL) _nLevel {
    if (_nLevel == BABY)
        return @"Busy Bee";
    if (_nLevel == DRONE)
        return @"Drone Bee";
    if (_nLevel == WORKER)
        return @"Worker Bee";
    if (_nLevel == BUMBLE)
        return @"Bumble Bee";
    if (_nLevel == QUEEN)
        return @"Queen/King Bee";
    
    return @"Busy Bee";
}

+ (NSString*) getDesc:(BUSY_LEVEL) _nLevel  {
    if (_nLevel == BABY)
        return @"Welcome to Busy Bees!";
    if (_nLevel == DRONE)
        return @"Your making everyone's easier!";
    if (_nLevel == WORKER)
        return @"You've established yourself as an honest bee!";
    if (_nLevel == BUMBLE)
        return @"You're the ultimate busy bee!";
    if (_nLevel == QUEEN)
        return @"Everyone loves you and works to bee you!";
    
    return @"Welcome to Busy Bees!";
}

+ (UIColor*) getBusyColor:(BUSY_LEVEL) nLevel {
    UIColor *color = UIColorFromRGB(BABY_COLOR);
    
    if (nLevel == BABY) {
        color = UIColorFromRGB(BABY_COLOR);
    }
    if (nLevel == DRONE) {
        color = UIColorFromRGB(DRONE_COLOR);
    }
    if (nLevel == WORKER) {
        color = UIColorFromRGB(WORKER_COLOR);
    }
    if (nLevel == BUMBLE) {
        color = UIColorFromRGB(BUMBLE_COLOR);
    }
    if (nLevel == QUEEN) {
        color = UIColorFromRGB(QUEEN_COLOR);
    }
    
    return color;
}

@end