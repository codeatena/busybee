//
//  FriendsViewController.h
//  Busybee
//
//  Created by User on 4/19/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView*   myTableView;
    NSMutableArray*         arrData;
}

- (IBAction)onBack:(id)sender;

@end
