//
//  FriendsViewController.m
//  Busybee
//
//  Created by User on 4/19/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendTableViewCell.h"
#import "GlobalData.h"
#import "ProfileViewController.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"friends_profile_segue"]) {
        ProfileViewController* profileViewCtrl = (ProfileViewController*) segue.destinationViewController;
        profileViewCtrl.user = (User*) sender;
    }
}

#pragma mark - init

- (void) initWidget {
    myTableView.separatorColor = [UIColor clearColor];
}

- (void) initData {
    arrData = [[NSMutableArray alloc] init];
    arrData = [[[GlobalData sharedInstance] curUser] arrFriends];
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendTableViewCell"];
    
    User *user = [arrData objectAtIndex:indexPath.row];
    [cell setFriend:user];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = [arrData objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"friends_profile_segue" sender:user];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark IBActions

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
