//
//  BuzzingViewController.h
//  Busybee
//
//  Created by User on 4/18/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UILabel*           lblMostPopular;
    IBOutlet UITableView*       myTableView;
    NSMutableArray*             arrData;
}

- (IBAction)onBack:(id)sender;

@end
