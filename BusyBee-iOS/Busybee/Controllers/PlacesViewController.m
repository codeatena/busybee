//
//  PlacesViewController.m
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "PlacesViewController.h"
#import "PlaceTableViewCell.h"
#import "PlaceDetailViewController.h"
#import "ProfileViewController.h"
#import "RankingViewController.h"
#import "Constants.h"
#import "GlobalData.h"
#import "Foursquare2.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface PlacesViewController ()

@end

@implementation PlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initNotification];
    [self initWidget];
    [self initData];
    [self initLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init

- (void) initNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:NOTIFY_MENU_CLICK_PROFILE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:NOTIFY_MENU_CLICK_RANKING object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:NOTIFY_MENU_CLICK_FRIENDS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:NOTIFY_MENU_CLICK_BUZZING object:nil];
}

- (void)receiveNotification:(NSNotification *)notification {
    if ([notification.name isEqualToString:NOTIFY_MENU_CLICK_PROFILE]) {
        [self onProfile];
    }
    if ([notification.name isEqualToString:NOTIFY_MENU_CLICK_RANKING]) {
        [self onRanking];
    }
    if ([notification.name isEqualToString:NOTIFY_MENU_CLICK_FRIENDS]) {
        [self onFriends];
    }
    if ([notification.name isEqualToString:NOTIFY_MENU_CLICK_BUZZING]) {
        [self onBuzzing];
    }
}

- (void) initWidget {
    [self initTableView];
    [self initSearchBar];
    [self initNavigation];
    [self initMenu];
}

- (void) initLocation {
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined ) {
            // We never ask for authorization. Let's request it.
            [locationManager requestWhenInUseAuthorization];
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                   [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
            // We have authorization. Let's update location.
            [locationManager startUpdatingLocation];
        } else {
            // If we are here we have no pormissions.
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No athorization"
                                                                message:@"Please, enable access to your location"
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"Open Settings", nil];
            [alertView show];
        }
    } else {
        // This is iOS 7 case.
        [locationManager startUpdatingLocation];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

- (void) initTableView {
    myTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    myTableView.separatorColor = [UIColor clearColor];
}

- (void) initSearchBar {
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
}

- (void) initData {
    arrData = [[NSMutableArray alloc] init];
    arrData = [[GlobalData sharedInstance] arrPlaces];
    
    if ([GlobalData sharedInstance].isNewUserbyFacebook) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 
                 [GlobalData sharedInstance].curUser.pUser.email = result[@"email"];
                 [GlobalData sharedInstance].curUser.pUser[@"firstName"] = result[@"first_name"];
                 [GlobalData sharedInstance].curUser.pUser[@"lastName"] = result[@"last_name"];
                 [GlobalData sharedInstance].curUser.pUser.username = result[@"email"];
                 
                 [[GlobalData sharedInstance].curUser.pUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                     if (!error) {
                         NSLog(@"your account is updated.");
                         
                     } else {
                         NSLog(@"your account is not updated.");
                     }
                 }];
             } else {
                 NSLog(@"%@", error);
             }
         }];
    }

    [myTableView reloadData];
}

- (void) initMenu {
    [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender{

    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];

    [self.frostedViewController panGestureRecognized:sender];
}

- (void) initNavigation {
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(BLACK_COLOR)];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id) sender {
    if ([segue.identifier isEqualToString:@"place_detail_segue"]) {
        PlaceDetailViewController* placeDetaiViewCtrl = (PlaceDetailViewController*) segue.destinationViewController;
        placeDetaiViewCtrl.place = (Place*) sender;
    }
    if ([segue.identifier isEqualToString:@"places_profile_segue"]) {
        ProfileViewController* profileViewCtrl = (ProfileViewController*) segue.destinationViewController;
        profileViewCtrl.user = [[GlobalData sharedInstance] curUser];
    }
    if ([segue.identifier isEqualToString:@"places_ranking_segue"]) {
        RankingViewController* rankingViewCtrl = (RankingViewController*) segue.destinationViewController;
        rankingViewCtrl.user = [[GlobalData sharedInstance] curUser];
    }
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceTableViewCell"];
    
    Place* place = [arrData objectAtIndex:indexPath.row];
    [cell setPlace:place];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Place *place = [arrData objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"place_detail_segue" sender:place];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView*) tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static PlaceTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    Place *place = [arrData objectAtIndex:indexPath.row];
    dispatch_once(&onceToken, ^{
        sizingCell = [tableView dequeueReusableCellWithIdentifier:@"PlaceTableViewCell"];
    });
    
    [sizingCell setPlace:place];
    
    return [self calculateHeightForConfiguredSizingCell:tableView cell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableView*) tableView cell:(UITableViewCell*) sizingCell {
    
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.frame), CGRectGetHeight(sizingCell.bounds)); // missing line
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

#pragma mark - location manager delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [locationManager stopUpdatingLocation];
    [self getVenuesForLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"Location manager did fail with error %@", error);
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
}

- (void)getVenuesForLocation:(CLLocation *)location {
    
    [Foursquare2 venueSearchNearByLatitude:@(location.coordinate.latitude)
                                 longitude:@(location.coordinate.longitude)
                                     query:nil
                                     limit:nil
                                    intent:intentCheckin
                                    radius:@(500)
                                categoryId:nil
                                  callback:^(BOOL success, id result){
                                      if (success) {
                                          NSDictionary *dic = result;
                                          NSLog(@"%@", dic);
//                                          NSArray *venues = [dic valueForKeyPath:@"response.venues"];
//                                          FSConverter *converter = [[FSConverter alloc]init];
//                                          self.nearbyVenues = [converter convertToObjects:venues];
//                                          [self.tableView reloadData];
//                                          [self proccessAnnotations];
                                          
                                      }
                                  }];
    
    [Foursquare2 venueGetDetail:@"42bf4180f964a520b1251fe3" callback:^(BOOL success, id result) {
        NSDictionary *dic = result;
        NSLog(@"%@", dic);
    }];
}

#pragma mark - searchBar Delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = @"";
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

#pragma mark - IBActions

- (IBAction)onMenu:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];

    [self.frostedViewController presentMenuViewController];
}

- (IBAction)onStar:(id)sender {
    
}

- (void) onProfile {
    [self performSegueWithIdentifier:@"places_profile_segue" sender:nil];
}

- (void) onRanking {
    [self performSegueWithIdentifier:@"places_ranking_segue" sender:nil];
}

- (void) onFriends {
    [self performSegueWithIdentifier:@"places_friends_segue" sender:nil];
}

- (void) onBuzzing {
    [self performSegueWithIdentifier:@"places_buzzing_segue" sender:nil];
}

@end
