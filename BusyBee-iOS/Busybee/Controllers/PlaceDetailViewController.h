//
//  PlaceDetailViewController.h
//  Busybee
//
//  Created by User on 4/5/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class Place;

@interface PlaceDetailViewController : UIViewController {
    
    IBOutlet MKMapView* mapView;
    IBOutlet UILabel*   lblBusy;
    IBOutlet UILabel*   lineBusy;
    IBOutlet UILabel*   lblAddress;
    IBOutlet UILabel*   lblPhone;
    IBOutlet UILabel*   lblOpenHours;
    
    IBOutlet UIButton*  btnDirections;
    IBOutlet UIButton*  btnWriteReview;
    
    IBOutlet UIImageView*   imvHowBusy;
    IBOutlet UILabel*       lblTypical;
    IBOutlet UILabel*       lblDailyBack;
    IBOutlet UIImageView*   imvDaily;
    
    IBOutlet UILabel*       lblTime1;
    IBOutlet UILabel*       lblTime2;
    IBOutlet UILabel*       lblTime3;
    IBOutlet UILabel*       lblTime4;
    IBOutlet UILabel*       lblTime5;
    IBOutlet UILabel*       lblTime6;
    IBOutlet UILabel*       lblTime7;
    IBOutlet UILabel*       lblTime8;
    
    IBOutlet UISlider*      sldHowBusy;
    IBOutlet UIButton*      btnTitle;
}

@property (nonatomic, strong) Place* place;

- (IBAction)onDirections:(id)sender;
- (IBAction)onWriteReview:(id)sender;
- (IBAction)onBack:(id)sender;
- (IBAction)onStar:(id)sender;

@end
