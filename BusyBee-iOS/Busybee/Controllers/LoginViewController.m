//
//  LoginViewController.m
//  Busybee
//
//  Created by User on 3/25/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "LoginViewController.h"
#import "Constants.h"
#import <Parse/Parse.h>
#import "SVProgressHUD.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GlobalData.h"

@interface LoginViewController () <UITextFieldDelegate> {
    IBOutlet UITextField* tfEmail;
    IBOutlet UITextField* tfPassword;
    IBOutlet UIButton*  btnLogin;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - init

- (void) initWidget {
    [btnLogin setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnLogin setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
}

- (void) initData {
    tfEmail.text = @"hcw1987@hotmail.com";
    tfPassword.text = @"aaaa";
    
    [GlobalData sharedInstance].isNewUserbyFacebook = NO;
}

#pragma mark - textField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - login functions

- (void) successLogin  {
    [self performSegueWithIdentifier:@"login_places_segue" sender:nil];
}

#pragma mark - IBActions

- (IBAction)onFacebook:(id)sender {
//    [self successLogin];
    [PFFacebookUtils logInInBackgroundWithReadPermissions:@[@"email", @"public_profile", @"user_friends"] block:^(PFUser *pUser, NSError *error) {
        if (!pUser) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else {
            
            [[GlobalData sharedInstance].curUser setPUser:pUser];
            
            if (pUser.isNew) {
                NSLog(@"User signed up and logged in through Facebook!");
                
                [GlobalData sharedInstance].isNewUserbyFacebook = YES;
                //TODO: process error or result
            } else {
                NSLog(@"User logged in through Facebook!");
            }
            [self successLogin];
        }
    }];
}

- (IBAction)onGoogle:(id)sender {
    [self successLogin];
}

- (IBAction)onLogin:(id)sender {
//    [self successLogin];
    if ([tfEmail.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfPassword.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [PFUser logInWithUsernameInBackground:tfEmail.text password:tfPassword.text
                                    block:^(PFUser *pUser, NSError *error) {
                                        if (pUser) {
                                            NSLog(@"logged in user name %@ %@", pUser[@"firstName"], pUser[@"lastName"]);
                                            [SVProgressHUD dismiss];
                                            
                                            [[GlobalData sharedInstance].curUser setPUser:pUser];
                                            [self successLogin];
                                        } else {
                                            [SVProgressHUD showErrorWithStatus:@"Your email address or password is invalid."];
                                        }
                                    }];
}

- (IBAction)onCreateNewAccount:(id)sender {
    [self performSegueWithIdentifier:@"login_signup_segue" sender:nil];
}

@end