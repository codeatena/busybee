//
//  PlacesViewController.h
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface PlacesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CLLocationManagerDelegate, UIAlertViewDelegate> {
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrData;
    
    CLLocationManager *locationManager;
}

- (IBAction)onMenu:(id)sender;
- (IBAction)onStar:(id)sender;

@end
