//
//  RankingViewController.m
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "RankingViewController.h"
#import "ImageUtility.h"
#import "SimpleTableViewCell.h"
#import "Constants.h"
#import "Level.h"
#import "GlobalData.h"
#import "NumberUtility.h"

@interface SLExpandableTableViewControllerHeaderCell : UITableViewCell <UIExpandingTableViewCell>

@property (nonatomic, assign, getter = isLoading) BOOL loading;
@property (nonatomic, readonly) UIExpansionStyle expansionStyle;
@property (nonatomic, strong) UILabel* lblSectionTitle;

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated;

@end

@implementation SLExpandableTableViewControllerHeaderCell

@synthesize lblSectionTitle;

- (NSString *)accessibilityLabel {
    return self.textLabel.text;
}

- (void)setLoading:(BOOL)loading {
    if (loading != _loading) {
        _loading = loading;
        [self _updateDetailTextLabel];
    }
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {
    if (expansionStyle != _expansionStyle) {
        _expansionStyle = expansionStyle;
        [self _updateDetailTextLabel];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self _updateDetailTextLabel];
        self.backgroundColor = UIColorFromRGB(BLACK_COLOR);
        lblSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, RANK_CELL_HEIGHT - 1)];
        lblSectionTitle.textColor = UIColorFromRGB(YELLOW_COLOR);
        lblSectionTitle.font = [UIFont systemFontOfSize:13.0f];
        lblSectionTitle.textAlignment = NSTextAlignmentCenter;
        
        UILabel* lineSeparate = [[UILabel alloc] initWithFrame:CGRectMake(0, RANK_CELL_HEIGHT - 1, self.frame.size.width, 1)];
        lineSeparate.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:lblSectionTitle];
        [self addSubview:lineSeparate];
    }
    return self;
}

- (void)_updateDetailTextLabel {
    if (self.isLoading) {
        self.detailTextLabel.text = @"";
    } else {
        switch (self.expansionStyle) {
            case UIExpansionStyleExpanded:
                self.detailTextLabel.text = @"";
                break;
            case UIExpansionStyleCollapsed:
                self.detailTextLabel.text = @"";
                break;
        }
    }
}

@end

@interface RankingViewController ()

@end

@implementation RankingViewController

@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [expandableSections addIndex:0];
    [myTableView expandSection:0 animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - init

- (void) initWidget {
    UIImage *clearImage = [[UIImage alloc] init];
    
    [sldHowBusy setMinimumTrackImage:clearImage forState:UIControlStateNormal];
    [sldHowBusy setMaximumTrackImage:clearImage forState:UIControlStateNormal];
    
    UIImage* thumbImage = [UIImage imageNamed:@"thumb.png"];
    
    imvHowBusy.layer.borderWidth = 2.0f;
    imvHowBusy.layer.borderColor = [UIColorFromRGB(BLACK_COLOR) CGColor];
    
    [sldHowBusy setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(25, 25)] forState:UIControlStateNormal];
    [sldHowBusy setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(25, 25)] forState:UIControlStateHighlighted];
    
//    myTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    myTableView.separatorColor = [UIColor clearColor];
}

- (void) initData {
    
    arrData = [[NSMutableArray alloc] init];
    NSMutableArray* arrLevels = [[NSMutableArray alloc] init];
    NSMutableArray* arrPointScales = [[NSMutableArray alloc] init];
    
    SimpleData* simpleData = nil;
    
    simpleData = [[SimpleData alloc] initWithTitle:@"Baby Bee:" DESC:@"Welcome to Busy Bees!"];
    [arrLevels addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Drone Bee:" DESC:@"Your making everyone's easier!"];
    [arrLevels addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Worker Bee:" DESC:@"You've established yourself as an honest bee!"];
    [arrLevels addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Bumble Bee:" DESC:@"You're the ultimate busy bee!"];
    [arrLevels addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Queen/King Bee:" DESC:@"Everyone loves you and works to bee you!"];
    [arrLevels addObject:simpleData];
    
    [arrData addObject:arrLevels];
    
    simpleData = [[SimpleData alloc] initWithTitle:@"All Notifications:" DESC:@"+10 Points"];
    [arrPointScales addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Your First Bee notification:" DESC:@"+25 Bonus Points"];
    [arrPointScales addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Your 10th Bee notification:" DESC:@"+30 Bonus Points"];
    [arrPointScales addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"Your 50th Bee notification:" DESC:@"+50 Bonus Points"];
    [arrPointScales addObject:simpleData];
    simpleData = [[SimpleData alloc] initWithTitle:@"100% accuracy:" DESC:@"+50 Bonus Points"];
    [arrPointScales addObject:simpleData];
    
    [arrData addObject:arrPointScales];
    
    lblCurLevel.text = [Level getTitle:user.nLevel];
    lblPoints.text = [NSString stringWithFormat:@"%@ Points", [NumberUtility getFormattedStringFromInteger: user.nPoint]];
    lblCurRank.text = [NSString stringWithFormat:@"%@ out of %@ local users", [NumberUtility getFormattedStringFromInteger:user.nRank], [NumberUtility getFormattedStringFromInteger:[GlobalData sharedInstance].nLocalUserCount]];
    lblWorldRank.text = [NSString stringWithFormat:@"%@ out of %@ users", [NumberUtility getFormattedStringFromInteger:user.nWorldRank], [NumberUtility getFormattedStringFromInteger:[GlobalData sharedInstance].nAllUserCount]];
    
    expandableSections = [NSMutableIndexSet indexSet];
}

#pragma mark - SLExpandableTableViewDatasource

- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
//    return ![expandableSections containsIndex:section];
    return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SLExpandableTableViewControllerHeaderCell";
    SLExpandableTableViewControllerHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[SLExpandableTableViewControllerHeaderCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
//    cell.textLabel.text = [NSString stringWithFormat:@"Section %ld", (long)section];
//    cell.textLabel.textColor = UIColorFromRGB(BLACK_COLOR);
    cell.lblSectionTitle.text = [NSString stringWithFormat:@"Section %ld", (long)section];

    if (section == 0) {
        cell.lblSectionTitle.text = @"LEVELS";
    }
    if (section == 1) {
        cell.lblSectionTitle.text = @"POINTS SCALE";
    }
    
    return cell;
}

#pragma mark - SLExpandableTableViewDelegate

- (void)tableView:(SLExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {

}

- (void)tableView:(SLExpandableTableView *)tableView didExpandSection:(NSUInteger)section animated:(BOOL)animated {
    [expandableSections addIndex:section];
    [expandableSections removeIndex:1 - section];
    [tableView collapseSection:1 - section animated:YES];
}

- (void)tableView:(SLExpandableTableView *)tableView didCollapseSection:(NSUInteger)section animated:(BOOL)animated {
    [expandableSections removeIndex:section];
    [expandableSections addIndex:1 - section];
    [tableView expandSection:1 - section animated:YES];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section > 0) {
//        return 44.0 * 2.0;
//    }
//    
//    return UITableViewAutomaticDimension;
//}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *dataArray = arrData[section];
    return dataArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleTableViewCell"];
    
    NSMutableArray* arrRow = [arrData objectAtIndex:indexPath.section];
    SimpleData* simpleData = [arrRow objectAtIndex:indexPath.row - 1];
    
    [cell setSimpleData:simpleData];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [arrData removeObjectAtIndex:indexPath.section];
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return RANK_CELL_HEIGHT;
}

#pragma mark IBActions

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
