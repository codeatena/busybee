//
//  ProfileViewController.h
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface ProfileViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UIView*        viewUserInf;
    IBOutlet UIImageView*   imvUserPhoto;
    IBOutlet UILabel*       lblUserAddress;
    IBOutlet UILabel*       lblUserReviewsCount;
    IBOutlet UILabel*       lblUserFriendsCount;
    
    IBOutlet UIView*        viewUserMoreInf;
    IBOutlet UILabel*       lblUserPoints;
    IBOutlet UILabel*       lblUserRank;
    IBOutlet UILabel*       lblUserBadges;
    
    IBOutlet UIView*        viewLastCheck;
    IBOutlet UILabel*       lblLastCheckTitle;
    IBOutlet UILabel*       lblLastCheckTime;
    
    IBOutlet UILabel*       lblHowBusy;
    
    IBOutlet UITableView*   myTableView;
    
    NSMutableArray*         arrData;
}

@property (nonatomic, strong) User* user;

- (IBAction)onBack:(id)sender;

@end
