//
//  RankingViewController.h
//  Busybee
//
//  Created by User on 4/15/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SLExpandableTableView.h>
#import "User.h"

@interface RankingViewController : UIViewController <SLExpandableTableViewDatasource, SLExpandableTableViewDelegate> {
    
    IBOutlet SLExpandableTableView* myTableView;
    
    IBOutlet UIImageView*   imvHowBusy;

    IBOutlet UILabel*       lblCurLevel;
    IBOutlet UILabel*       lblPoints;
    IBOutlet UILabel*       lblCurRank;
    IBOutlet UILabel*       lblWorldRank;
    
    IBOutlet UISlider*      sldHowBusy;
        
    NSMutableArray*         arrData;
    
    NSMutableIndexSet *expandableSections;
}

@property (nonatomic, strong) User* user;

- (IBAction)onBack:(id)sender;

@end
