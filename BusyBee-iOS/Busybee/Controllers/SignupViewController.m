//
//  SignupViewController.m
//  Busybee
//
//  Created by User on 4/20/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "SignupViewController.h"
#import "User.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>

@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark IBActions

- (IBAction)onSignup:(id)sender {
    if ([tfFirstName.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your first name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfLastName.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your last name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfEmail.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfPhoneNumber.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input your phone number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfPassword.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if ([tfConfirm.text isEqualToString:@""]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Please input confirm password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }
    if (![tfConfirm.text isEqualToString:tfPassword.text]) {
        UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Input Error!" message:@"Your confirm password is wrong." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerView show];
        return;
    }

    PFUser *pUser = [PFUser user];
    pUser.username = tfEmail.text;
    pUser.password = tfPassword.text;
    pUser.email = tfEmail.text;
    
    pUser[@"firstName"] = tfFirstName.text;
    pUser[@"lastName"] = tfLastName.text;
    pUser[@"phone"] = tfPhoneNumber.text;
    
    [SVProgressHUD showWithStatus:@"Please wait.."];

    [pUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"sigup success");
            [SVProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"singup error: %@", errorString);
            [SVProgressHUD showErrorWithStatus:errorString];
        }
    }];
}

#pragma mark - textField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
