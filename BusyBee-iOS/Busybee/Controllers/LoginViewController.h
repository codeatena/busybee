//
//  LoginViewController.h
//  Busybee
//
//  Created by User on 3/25/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

- (IBAction)onFacebook:(id)sender;
- (IBAction)onGoogle:(id)sender;
- (IBAction)onLogin:(id)sender;
- (IBAction)onCreateNewAccount:(id)sender;

@end
