//
//  DEMOMenuViewController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOMenuViewController.h"
#import "Constants.h"
#import "UIViewController+REFrostedViewController.h"

@interface DEMOMenuViewController ()

@end

@implementation DEMOMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor = UIColorFromRGB(MENU_BACK_COLOR);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = UIColorFromRGB(MENU_BACK_COLOR);
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 140.0f)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 50, 70, 70)];
//        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        imageView.image = [UIImage imageNamed:@"avatar.jpg"];
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 35.0;
        imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        imageView.layer.borderWidth = 2.0f;
        imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        imageView.layer.shouldRasterize = YES;
        imageView.clipsToBounds = YES;
        
        UILabel *lblUserName = [[UILabel alloc] initWithFrame:CGRectMake(120, 52, 0, 0)];
        lblUserName.text = @"John Stevens";
        lblUserName.font = [UIFont boldSystemFontOfSize:18];
        lblUserName.textColor = UIColorFromRGB(YELLOW_COLOR);
        [lblUserName sizeToFit];
        
        UILabel *lblRank = [[UILabel alloc] initWithFrame:CGRectMake(120, 75, 0, 0)];
        lblRank.text = @"Rank:";
        lblRank.font = [UIFont systemFontOfSize:15];
        lblRank.textColor = UIColorFromRGB(MENU_TEXT_COLOR);
        [lblRank sizeToFit];
        
        UILabel *lblRankValue = [[UILabel alloc] initWithFrame:CGRectMake(165, 75, 0, 0)];
        lblRankValue.text = @"123";
        lblRankValue.font = [UIFont boldSystemFontOfSize:15];
        lblRankValue.textColor = UIColorFromRGB(MENU_TEXT_COLOR);
        [lblRankValue sizeToFit];
        
        UILabel *lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(120, 95, 0, 0)];
        lblPoints.text = @"Points:";
        lblPoints.font = [UIFont systemFontOfSize:15];
        lblPoints.textColor = UIColorFromRGB(MENU_TEXT_COLOR);
        [lblPoints sizeToFit];
        
        UILabel *lblPointsValue = [[UILabel alloc] initWithFrame:CGRectMake(170, 95, 0, 0)];
        lblPointsValue.text = @"1024";
        lblPointsValue.font = [UIFont boldSystemFontOfSize:15];
        lblPointsValue.textColor = UIColorFromRGB(MENU_TEXT_COLOR);
        [lblPointsValue sizeToFit];
        
        [view addSubview:imageView];
        [view addSubview:lblUserName];
        [view addSubview:lblRank];
        [view addSubview:lblRankValue];
        [view addSubview:lblPoints];
        [view addSubview:lblPointsValue];

        view;
    });
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
//    
//    if (indexPath.section == 0 && indexPath.row == 0) {
//        DEMOHomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeController"];
//        navigationController.viewControllers = @[homeViewController];
//    } else {
//        DEMOSecondViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"secondController"];
//        navigationController.viewControllers = @[secondViewController];
//    }
//    
//    self.frostedViewController.contentViewController = navigationController;
    if (indexPath.row == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_MENU_CLICK_PROFILE object:nil];
    }
    if (indexPath.row == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_MENU_CLICK_RANKING object:nil];
    }
    if (indexPath.row == 4) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_MENU_CLICK_FRIENDS object:nil];
    }
    if (indexPath.row == 5) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_MENU_CLICK_BUZZING object:nil];
    }
    
    [self.frostedViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = UIColorFromRGB(YELLOW_COLOR);
    [cell setSelectedBackgroundView:bgColorView];
    
    cell.textLabel.highlightedTextColor = UIColorFromRGB(MENU_BACK_COLOR);
    
    CGRect curFrame = cell.textLabel.frame;
    cell.textLabel.frame = CGRectMake(curFrame.origin.x + 100, curFrame.origin.y, curFrame.size.width, curFrame.size.height);
    
    cell.backgroundColor = [UIColor clearColor];
    NSArray *titles = @[@"Home", @"My Profile", @"Points & Rank", @"Favorites", @"Friends", @"What's Buzzing?", @"Feed me (Recommendations)", @"Map", @"Tutorials"];
    cell.textLabel.text = titles[indexPath.row];
    cell.textLabel.textColor = UIColorFromRGB(MENU_TEXT_COLOR);

    return cell;
}
 
@end
