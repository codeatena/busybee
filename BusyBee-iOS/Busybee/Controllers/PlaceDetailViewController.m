//
//  PlaceDetailViewController.m
//  Busybee
//
//  Created by User on 4/5/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "PlaceDetailViewController.h"
#import "Place.h"
#import "Constants.h"
#import "Level.h"
#import "ImageUtility.h"
//#import "RWLabel.h"

@interface PlaceDetailViewController ()

@end

@implementation PlaceDetailViewController

@synthesize place;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidgets];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init

- (void) initWidgets {
    
    [btnTitle setTitle:place.title forState:UIControlStateNormal];
    lblBusy.backgroundColor = [Level getBusyColor:place.nLevel];
    //    lblBusy.layer.masksToBounds = YES;
    lblBusy.clipsToBounds = YES;
    lblBusy.layer.cornerRadius = lblBusy.frame.size.width / 2;

    lineBusy.backgroundColor = [Level getBusyColor:place.nLevel];
    
    lblAddress.text = place.address;

    lblPhone.text = place.displayPhone;
    lblOpenHours.text = [place getTodayOpenTime];
    
    [btnDirections setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    [btnWriteReview setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    
    [btnDirections setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    [btnWriteReview setTitleColor:UIColorFromRGB(YELLOW_COLOR) forState:UIControlStateNormal];
    
    imvHowBusy.layer.borderWidth = 2.0f;
    imvHowBusy.layer.borderColor = [UIColorFromRGB(SLIDER_BORDER_COLOR) CGColor];
    
    lblTypical.layer.borderWidth = 2.0f;
    lblTypical.layer.borderColor =  [UIColorFromRGB(SLIDER_BORDER_COLOR) CGColor];
    lblTypical.backgroundColor = [Level getBusyColor:place.nLevel];
    
    lblDailyBack.layer.borderWidth = 2.0f;
    lblDailyBack.layer.borderColor = [UIColorFromRGB(SLIDER_BORDER_COLOR) CGColor];
    lblDailyBack.backgroundColor = UIColorFromRGB(SLIDER_BACK_COLOR);
    
    UIImage *clearImage = [[UIImage alloc] init];
    
    [sldHowBusy setMinimumTrackImage:clearImage forState:UIControlStateNormal];
    [sldHowBusy setMaximumTrackImage:clearImage forState:UIControlStateNormal];
    
    UIImage* thumbImage = [UIImage imageNamed:@"thumb.png"];
    
    [sldHowBusy setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(25, 25)] forState:UIControlStateNormal];
    [sldHowBusy setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(25, 25)] forState:UIControlStateHighlighted];

//    [sldHowBusy setMinimumTrackImage:nil forState:UIControlStateNormal];
//    [sldHowBusy setMaximumTrackImage:nil forState:UIControlStateNormal];
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(place.lat, place.lng);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0, 360 / pow(2, 16) * mapView.frame.size.width / 256);
    [mapView setRegion:MKCoordinateRegionMake(center, span) animated:YES];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = center;
    point.title = place.title;
    
    [mapView addAnnotation:point];
    [mapView setCenterCoordinate:center animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBActions

- (IBAction)onDirections:(id)sender {
    
}

- (IBAction)onWriteReview:(id)sender {
    
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onStar:(id)sender {
    
}

@end