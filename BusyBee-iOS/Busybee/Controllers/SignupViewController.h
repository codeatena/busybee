//
//  SignupViewController.h
//  Busybee
//
//  Created by User on 4/20/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController {
    IBOutlet UITextField*           tfFirstName;
    IBOutlet UITextField*           tfLastName;
    IBOutlet UITextField*           tfEmail;
    IBOutlet UITextField*           tfPhoneNumber;
    IBOutlet UITextField*           tfPassword;
    IBOutlet UITextField*           tfConfirm;

    IBOutlet UIButton*              btnSignup;
}

- (IBAction)onSignup:(id)sender;

@end
