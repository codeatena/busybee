//
//  BuzzingViewController.m
//  Busybee
//
//  Created by User on 4/18/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "BuzzingViewController.h"
#import "BuzzingTableViewCell.h"
#import "GlobalData.h"
#import "PlaceDetailViewController.h"

@interface BuzzingViewController ()

@end

@implementation BuzzingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"buzzing_place_detail_segue"]) {
        PlaceDetailViewController* placeDetaiViewCtrl = (PlaceDetailViewController*) segue.destinationViewController;
        placeDetaiViewCtrl.place = (Place*) sender;
    }
}


#pragma mark - init

- (void) initWidget {
    [lblMostPopular setBackgroundColor:UIColorFromRGB(YELLOW_COLOR)];
    [lblMostPopular setTextColor:UIColorFromRGB(BLACK_COLOR)];
    
    myTableView.separatorColor = [UIColor clearColor];
}

- (void) initData {
    arrData = [[NSMutableArray alloc] init];
    arrData = [[GlobalData sharedInstance] arrPlaces];
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BuzzingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BuzzingTableViewCell"];
    
    Place* place = [arrData objectAtIndex:indexPath.row];
    [cell setPlace:place];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Place *place = [arrData objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"buzzing_place_detail_segue" sender:place];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark IBActions

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
