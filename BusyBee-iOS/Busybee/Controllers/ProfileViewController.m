//
//  ProfileViewController.m
//  Busybee
//
//  Created by User on 4/11/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "ProfileViewController.h"
#import "GlobalData.h"
#import "Constants.h"
#import "Checkin.h"
#import "SampleDataManager.h"
#import "CheckInTableViewCell.h"
#import "CustomDate.h"
#import "PlaceDetailViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"profile_place_detail_segue"]) {
        PlaceDetailViewController* placeDetaiViewCtrl = (PlaceDetailViewController*) segue.destinationViewController;
        placeDetaiViewCtrl.place = (Place*) sender;
    }
}


#pragma mark - init

- (void) initWidget {
    
    [viewUserInf setBackgroundColor:UIColorFromRGB(BLACK_COLOR)];
    
    [viewUserMoreInf setBackgroundColor:UIColorFromRGB(DARK_GRAY_COLOR)];
    [lblUserPoints setTextColor:UIColorFromRGB(YELLOW_COLOR)];
    [lblUserRank setTextColor:UIColorFromRGB(YELLOW_COLOR)];
    [lblUserBadges setTextColor:UIColorFromRGB(YELLOW_COLOR)];
    
    [viewLastCheck setBackgroundColor:[UIColor whiteColor]];
    [lblLastCheckTime setTextColor:UIColorFromRGB(GRAY_COLOR)];
    [lblHowBusy setBackgroundColor:UIColorFromRGB(YELLOW_COLOR)];
    
    imvUserPhoto.layer.cornerRadius = 45.0f;
    imvUserPhoto.layer.masksToBounds = YES;
    imvUserPhoto.layer.borderWidth = 2.0f;
    imvUserPhoto.layer.borderColor = [[UIColor whiteColor] CGColor];
}

- (void) initData {
    
    arrData = user.arrCheckin;
    imvUserPhoto.image = [UIImage imageNamed:user.strImageFileName];
    
    self.title = user.name;
    
    lblUserAddress.text =  user.address;
    lblUserReviewsCount.text = [NSString stringWithFormat:@"%lu Reviews", (unsigned long) user.arrReviews.count];
    lblUserFriendsCount.text = [NSString stringWithFormat:@"%lu Friends", (unsigned long) user.arrFriends.count];
    
    lblUserPoints.text = [NSString stringWithFormat:@"%lu", (unsigned long) user.nPoint];
    lblUserRank.text = [NSString stringWithFormat:@"%lu", (unsigned long) user.nRank];
    lblUserBadges.text = [NSString stringWithFormat:@"%lu", (unsigned long) user.nBadge];
    
    Checkin* lastCheckin = nil;
    
    if ([user.arrCheckin count] > 0) {
        lastCheckin = [user.arrCheckin objectAtIndex:user.arrCheckin.count - 1];
    }
    
    viewLastCheck.hidden = YES;
    lblLastCheckTitle.text = @"";
    lblLastCheckTime.text = @"";
    
    if (lastCheckin) {
        viewLastCheck.hidden = NO;
        lblLastCheckTitle.text = lastCheckin.place.title;
        lblLastCheckTime.text = [lastCheckin.date getDateString];
    }
    
    lblHowBusy.text = [NSString stringWithFormat:@"How Busy is %@", user.firstName];
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckInTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckInTableViewCell"];
    
    Checkin* checkin = [arrData objectAtIndex:indexPath.row];
    [cell setCheckin:checkin];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Checkin* checkin = [arrData objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"profile_place_detail_segue" sender:checkin.place];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark IBActions

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
